module.exports = function(grunt) {
    // grunt.registerTask('speak', function(){
    //     console.log("I'm Here!")
    // });
    grunt.initConfig({
        concat: {
            js: {
            src: ['js/script.js', 'js/jquery-2.1.3.min.js'],
            dest: 'final/js/script.js',
            },
            css: {
            src: ['css/style.css','css/responsive.css'],
            dest: 'final/css/style.css',
            }
        },
        autoprefixer:{
            options: {
                // We need to `freeze` browsers versions for testing purposes.
                browsers: ['opera 12', 'ff 15', 'chrome 25', 'ie 8', 'ie 9', 'ie 7']
            },
            single_file: {
                src: 'final/css/style.css',
                dest: 'final/css/style.css'
            },
        },
        browserSync: {
            bsFiles: {
                src : ['css/style.css','index.html']
            },
            options: {
                server: {
                    baseDir: "./"
                }
            }
        },
        watch: {
            js: {
                files: 'js/*.js',
                tasks: ['concat:js'],
            },
            css: {
                files: 'css/*.css',
                tasks: ['autoprefixer','concat:css','browserSync'],
            },
            html: {
                files: 'index.html',
                tasks: 'browserSync',
            },
        },
    });
    
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-autoprefixer');    
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-browser-sync');
};